# **1. Python**
* **what** : strongly typed, dynamically
- **Pros** : 語法簡單、工法完整、應用廣泛
- **Cons** : 執行緩慢  
原因有三 : GIL（Global Interpreter Lock 全局解釋器鎖)、解釋型語言、動態類型語言<br>
https://buzzorange.com/techorange/2018/08/14/python-is-slow/

- **Introspection** : 
<br>http://zetcode.com/lang/python/introspection/

- **2.X vs 3.X** :  
    1. print
    2. unicode
    3. /
    4. exception
    5. xrange
    6. 八進位寫法
    7. 不等
    8. repr
    9. map、filter、zip 和 reduce  
- **List Comprehensions vs generator expressions** :  
    * generator 是一種迭代器
    * generator 使用yield返回生成器
    * 既然生成器表達式比列表解析快？那為什麼不全部使用生成器表達式？A: 1.最後你還是要一個列表2.你需要排序
 ```python
    #[ expression for item in list if conditional ]List Comprehensions 的寫法等同於以下
    for item in list:
    if conditional:
        expression
```
- **PS**:
    - **statically**：編譯的時候就設定好每一變量類型，因為類型錯誤產生的是語法錯誤SYNTAX ERROR。
    - **dynamically**：編譯的時候不用設定好每一變量類型，因為類型錯誤產生的是運行時才產生錯誤。

    - **object-oriented programming,OOP** :  
        * object--宇宙間任何具體的東西或抽象的事物
            * 三個主要特徵來描述物件(Object):
                * 狀態(State)：指物件各種特性的現況
                * 行為(Behavior)：指物件的功能。
                * 身份(Identity)[Grady Booch, 1991]：身份用來標示一個物件，可能是一個名稱或是號碼
            * Examples:
                * 車子:
                    * State:速度，排氣量，顏色，重量，幾人座
                    * Behavior : 油門,方向盤,煞車
                    * Identity: A先生擁有的汽車
                * 會議:
                    * State :開會日期,地點,議題,出席人員
                    * Behavior :決策
                    * Identity :A公司的每月例行會議  
        
        物件導向一般有下列的特徵： 
        * Class：定義了物件的抽象模板與結構，擁有屬性與方法。
        * Object：物件是類別的實體（instance），可以使用類別中定義的方法。
        * Inheritance：類別彼此可以繼承，子類別可以使用母類別的屬性與方法。
        * Encapsulation：每一個方法就像一個黑盒子，不需要知道具體如何實作，只要知道使用方法即可。

    - **functional programming** : 函數導向非常在意「沒用副作用」這個精神，相同的輸入一定要有相同的結果。並且利用這個特性，將一個複雜的問題，不斷的透過純函式逐層推導出複雜的運算，而不是設計一個相對複雜的執行程序。函數導向的特性：
        * 避免改變狀態
        * 避免可變的資料，沒有任何副作用
        * 純函式：傳入相同的參數會得到相同的結果
        * 延遲評估 (Lazy evaluation)：參數傳入時才執行，不會預先載入
    - **CPU密集型 vs IO密集型** : https://www.jianshu.com/p/1f5195dcc75b  

# **2. About coding** :
1. *args,**kwargs
2. @decorator AOP
3. @classmethod @staticmethod @property
4. lambda
5. deepcopy
```python
a={'a':'aa','b':'bb'}
b=a
# id(a) = id(b)
a['a']='aaaaa'
# id(a) = id(b)，且 b={'a':'aaaaa','b':'bb'}
```
6. and or
7. mutliple inheritance ,MRO(Method Resulotion Order)
8. 運算子:(//,%,**)、賦值運算子(//=)、邏輯運算子(and,or,not)、成員運算子(in,not in)、身分運算子(is,is not)、位元運算子(&,|,^,~,<<,>>)
9. tuple 的解、封裝
10. 一行內聲明並賦值多個變數
11. dict: keys()
12. 多進位: int() bin() oct() hex()
13. String: upper() lower() isupper() islower() istitle() strip() join() split()
14. from random import shuffle
15. class method :`__call__`、`__class__`
16. new 和 init:`__new__` 方法，可以决定返回對象，也就是創建之前，這個可以用在設計模式的單例、工廠模式
17. 如何實現interface、abstract，@abstractmethod-https://openhome.cc/Gossip/Python/AbstractClass.html
18. django為什麼是OOP的框架-https://www.quora.com/Is-Django-a-way-to-do-OOP  
19. itertools -http://funhacks.net/2017/02/13/itertools/  
    itertools 提供了很多類型迭代器的函数，但返回值不是list，而是迭代器。  
    * 無限迭代器(Infinite Iterators)：生成一個無限序列；  
        * count(firstval=0, step=1)  ex:count(10,2) 結果:10,12,14...  
        * cycle(iterable)  ex:cycle('ABC') 結果:A,B,C,A,B,C,A,B,C...  
        * repeat(object [,times])  ex:itertools.repeat([1, 2, 3, 4], 3) 結果:[1, 2, 3, 4],[1, 2, 3, 4],[1, 2, 3, 4]  
    * 有限迭代器：接收一個或多個序列（sequence）作為參數，進行组合、分组和過濾等；  
        * chain() 連接
        * compress() compress('ABCDEF', [1, 1, 0, 1, 0, 1])，T/F篩選器
        * dropwhile() dropwhile(lambda x: x < 5, [1, 3, 6, 2, 1])，當符合條件丟掉一直到第一個丟不掉
        * groupby() 分組
        * ifilter()& ifilterfalse(), filter() in `python3`
        * islice() islice(iterable, [start,] stop [, step])
        * imap(), map() in `python3`
        * tee(),tee(iterable [,n])，創造n個
        * takewhile() 跟dropwhile相反
        * izip(),zip() in `python3`，拉鍊
        * izip_longest ,zip_longest in `python3`，拉鍊填滿
    * 组合生成器(Combinatoric generators)：排列組合；  
        * product : product(iter1, iter2, ... iterN, [repeat=1])  ex: product('ABCD', 'xy')、product('ab', range(3))、product('ABC', repeat=2)  
        * permutations : permutations(iterable[, r])  ex: permutations('ABC', 2)、permutations('ABC')
        * combinations : 有點像C，combinations(iterable, r)
        * combinations_with_replacement : combinations_with_replacement('ABC', 2)
- **優化技巧** :  
    1. `dict`有keys所以搜尋會比List快。  
    2. 在巢狀for迴圈時`避免在最內圈進行計算`，可使用temp在外層先存取。  
    3. 直接用`SET()`集合去處理(union'|'， intersection'&'，difference'-')會比list迭代快  
    4. 如果需要交换两个变量的值使用 `a,b=b,a` 而不是借助中间变量 t=a;a=b;b=t；  
    5. 在循環的時候使用`xrange` 而不是 range；使用 xrange可以節省系統內存，因為xrange() 在序列中每次只會產生一個整數。而 range() 將直接返回完整的元素列表，在loop的時候會消耗過多內存。在python3 沒有 xrange，range 提供一个可以遍歷任意長度的 iterator。  
    6. 使用局部變數，避免`"global"`關鍵字。python訪問局部變數比全局變數要快得多。  
    7. `if done is not None` 比 if done != None 更快；但`is跟==並不一樣!!`  
    8. 在耗時較多的循環中，可以把函数的調用改為內聯的方式；  
    9. 使用 `"x < y < z"` 而不是 "x < y and y < z"；  
    10. ` while 1` 要比 while True 更快;  
    11. `build-in` 函数通常比較快，add(a,b) 快於 a+b。  -https://www.ibm.com/developerworks/cn/linux/l-cn-python-optim/index.html  
# **3. monkey patch** :  
Monkey Patch 就是在 run time 時動態更改 class 或是 module 已經定義好的函數或是屬性內容。實務上常見的使用在 test 上用來 mock 行為或是 gevent 函式庫等。
```python
class Foo(object):
    def bar(self):
        print('Foo.bar')
def bar(self):
    print('Modified bar')
Foo().bar()
Foo.bar = bar
Foo().bar()
# Foo.bar
# Modified bar
```
**例子 :**  
很多代碼用到 import json，後來發現ujson性能更高，如果覺得把每個文件的import json 改成 import ujson as json成本較高，或者說想測試一下用ujson替換json是否符合預期，只需要在入口加上：
```python
import json
import ujson
def monkey_patch_json():
    json.__name__ = 'ujson'  
    json.dumps = ujson.dumps  
    json.loads = ujson.loads  
```

# **4. multi-threading,GIL(global interpreter lock)** :  
* **GIL what?**  
http://cenalulu.github.io/python/gil-in-python/  
GIL是一把全局排他鎖。毫無疑問全局鎖的存在會對多線程的效率有不小影響。甚至就幾乎等於Python是單線程。  
*In CPython, the global interpreter lock, or GIL, is a mutex that prevents multiple native threads from executing Python bytecodes at once. This lock is necessary mainly because CPython’s memory management is not thread-safe. (However, since the GIL exists, other features have grown to depend on the guarantees that it enforces.)*  
**GIL 為什麼使用why?**  
為了利用多核，Python開始支持多線程。而解决多線程之間數據完整性和狀態同步的最簡單方法自然就是加上鎖。 於是有了GIL這把超級大鎖，而越來越多的代碼庫開發者接受了GIL以後，他们開始依賴這種特性（即默認python内部對象是thread-safe的，無需考慮額外的内存鎖和同步操作）。  
* **GIL問題**  
Python的多線程在多核CPU上，只對於IO密集型計算產生正面效果；當至少有一個CPU密集型存在，效率會由於GIL而大幅下降。GIL只會影響到那些嚴重依賴CPU的程序（計算型）。 如果你的程序大部分只會涉及到I/O，那使用多線程就很適合， 因為大部分時間都在等待。  
* **multiprocessing**  
multiprocessing vs threading  
https://hk.saowen.com/a/99684a9b988e413089471a14c31383e63489498996b46f6502c6c05a0d8d621c  
因為有GIL的存在，python threading 是假的多線程，正是這個鎖能保證同時只有一個線程在運行。  
multiprocessing則可以解決此問題，而Django也是採用多進程。  
https://morvanzhou.github.io/tutorials/python-basic/multiprocessing/4-comparison/  

# **5. 內存管理** :  
https://blog.csdn.net/jiangjiang_jian/article/details/79140742  
* **垃圾回收** :  
1. 當内存中有不再使用的部分時，垃圾收集器就會把他們清理掉。它會去檢查那些引用計數0的對象，然後清除其在内存的空間。另一情况也會被垃圾收集器清掉：當兩個對象相互引用時，他們本身其他的引用已經為0。  
2. 垃圾回收機制還有一個循環垃圾回收器, 確保釋放循環引用對象(a引用b, b引用a,導致其引用計數永遠不為0)。 

Ruby與Python的垃圾回收 - https://www.jianshu.com/p/1e375fb40506   
python採用引用計數為主，標記－清除和分代收集兩種機制為輔的策略。  
**採用引用計數的缺點** : 1.需要在每個對象內預留空間來處理引數。2.需要複雜的操作引數的增加減少。3.相對慢，在大數據結構時，會需要很多操作。  
**分代收集Generational Garbage Collection**

* **引用計數** :
變量更像是附在對象上的標籤。當變量被绑定在一個對象上的時候，該變量的引用計數就是1，系统會自動維護標籤，定時掃描，當引用計數變為0的時候，就會被回收。  
```python
from sys import getrefcount
a = [1, 2, 3]
print(getrefcount(a)) #2
b = a
print(getrefcount(b)) #3
```
```python
import gc
print(gc.get_threshold())#(700,10,10)處理垃圾回收的閥值
```
* **內存池**

# **6. TDD** :  
* TDD(Test-Driven Development): 
`先寫測試再開發程式`，其中一個最大的好處是開發者當寫出測試時，就可以瞭解這一個元件最後會怎麼使用，同時也釐清的程式的介面會如何定義。  
當我們沒有測試憑空開出的 API 介面，常常會設計好了真的使用時又發現不合用，會需要修改個幾次確定到底要怎麼開才好。當在沒有實作前就寫了測試，在開發前就會對整個介面有更清楚的理解，寫出來的介面變動就可以更小。  
而開發者之間有需要討論時，也可以用這份測試案例討論到底該怎麼實作好，作為討論的基礎，在還程式還沒實作前就可以確認介面跟修改，花費的成本與時間會比實作後來得低。  
`Write Failing Test-->Run and Fail Test-->Write code to Pass-->Run and Pass-->Refactor-->Write F...Repeat...`  
* BDD (Behavior-driven development):  
BDD 則是比起 TDD 更進一步，除了在實作前先寫測試外，而在測試前還要`先寫規格`，但是這份規格並不是單純的敘述軟體的功能，而是這份規格，是一份「可以執行的規格」。  
https://medium.com/@yurenju/%E8%87%AA%E5%8B%95%E8%BB%9F%E9%AB%94%E6%B8%AC%E8%A9%A6-tdd-%E8%88%87-bdd-464519672ac5  

# **7. Django** :  
master  
URL引數說明(包含1跟2差別):-
https://hk.saowen.com/a/6d6d77181cafe2422413dab0cc1d4e72db74efac2bdb2521c806f5cf6c961f8e
https://tw.saowen.com/a/70ba005f381a3c704af6dd1e07a8bbe505071976107fb415cbad89f37c16a773
http://dokelung-blog.logdown.com/posts/234896-django-notes-11-permission-and-registration
# **8. git flow** :  https://ithelp.ithome.com.tw/users/20004901/ironman/525
stash:1.apply:目前有stash可以直接
