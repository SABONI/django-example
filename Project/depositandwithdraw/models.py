from django.db import models


class Deposits(models.Model):
    memuuid = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp', null=True)
    status = models.IntegerField(default=0)
    coin = models.ForeignKey('management.Coins', on_delete=models.CASCADE)
    qty = models.CharField(db_column='qty', max_length=20)
    address = models.CharField(db_column='address', max_length=50, null=True)
    txhash = models.CharField(db_column='txhash', max_length=50, null=True)
    chain = models.CharField(db_column='chain', max_length=50, null=True)


class Withdrawals(models.Model):
    memuuid = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp', null=True)
    status = models.IntegerField(default=0)
    coin = models.ForeignKey('management.Coins', on_delete=models.CASCADE)
    qty = models.CharField(db_column='qty', max_length=20)
    address = models.CharField(db_column='address', max_length=50, null=True)
    txhash = models.CharField(db_column='txhash', max_length=50, null=True)
    chain = models.CharField(db_column='chain', max_length=50, null=True)
    comfirm_time = models.CharField(db_column='comfirm_time', max_length=20)
