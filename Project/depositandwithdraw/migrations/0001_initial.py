# Generated by Django 2.1.3 on 2018-11-29 06:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('management', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deposits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp', null=True)),
                ('status', models.IntegerField(default=0)),
                ('qty', models.CharField(db_column='qty', max_length=20)),
                ('address', models.CharField(db_column='address', max_length=50, null=True)),
                ('txhash', models.CharField(db_column='txhash', max_length=50, null=True)),
                ('chain', models.CharField(db_column='chain', max_length=50, null=True)),
                ('coin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='management.Coins')),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Withdrawals',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp', null=True)),
                ('status', models.IntegerField(default=0)),
                ('qty', models.CharField(db_column='qty', max_length=20)),
                ('address', models.CharField(db_column='address', max_length=50, null=True)),
                ('txhash', models.CharField(db_column='txhash', max_length=50, null=True)),
                ('chain', models.CharField(db_column='chain', max_length=50, null=True)),
                ('comfirm_time', models.CharField(db_column='comfirm_time', max_length=20)),
                ('coin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='management.Coins')),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
