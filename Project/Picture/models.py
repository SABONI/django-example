from django.db import models


class Items(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name
'''
    @models.permalink
    def get_absolute_url(self):
        return ('item_detail', None, {'object_id': self.id})
'''

class Photos(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='photos')
    caption = models.CharField(max_length=250, blank=True)

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return self.title
'''
    @models.permalink
    def get_absolute_url(self):
        return ('photo_detail', None, {'object_id': self.id})
'''

class Photos2(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    image = models.ImageField()
    caption = models.CharField(max_length=250, blank=True)

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return self.title
'''
    @models.permalink
    def get_absolute_url(self):
        return ('photo_detail', None, {'object_id': self.id})
'''