from django.shortcuts import render
from django.core.files.base import ContentFile
from Picture.models import Items, Photos, Photos2


def picture(request):
    data = request.get_full_path()
    if request.method == 'POST':
        if request.POST.get('aaa'):
            file_content = ContentFile(request.FILES['picture'].read())
            item = Items(name=request.POST['name'])
            item.save()
            img = Photos(item=item, title=request.FILES['picture'].name, image=request.FILES['picture'])
            img.save()
        else:
            file_content = ContentFile(request.FILES['picture'].read())
            item = Items(name=request.POST['name'])
            item.save()
            img = Photos2(item=item, title=request.FILES['picture'].name, image=request.FILES['picture'])
            img.save()
    if Photos2.objects.filter(pk=2):
        im = Photos2.objects.filter(pk=2)[0].image
    return render(request, 'load_images.html', locals())
