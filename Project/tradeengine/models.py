from django.db import models


class Orders(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    timestamp = models.IntegerField(db_column='timestamp')
    oid = models.BigAutoField(db_column='oid', max_length=50, primary_key=True)
    account = models.CharField(db_column='account', max_length=50)
    side = models.CharField(db_column='side', max_length=20)
    price = models.FloatField(db_column='price', max_length=20)
    qty = models.FloatField(db_column='qty', max_length=20)
    status = models.IntegerField(default=0)  # 狀態 0掛單1掛單-交易完成2即時-交易完成-3取消
    cate = models.CharField(db_column='cate', max_length=20)
    symbol = models.ForeignKey('management.Exchangeinfos', on_delete=models.CASCADE)

    def order_total(self):
        return self.price*self.qty


class Deals(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp')
    oid = models.ForeignKey('Orders', on_delete=models.CASCADE)
    txn_price = models.FloatField(db_column='price', max_length=20)
    txn_qty = models.FloatField(db_column='qty', max_length=20)
