"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import re_path, include,path

from accounts.views import login, register, user_index, open_2FA, index, logout, change_password,regist_email_verify, forgot_password,email_verify
from management.views import detail,create,modify,terminate,alllist
from Picture.views import picture

from rest_framework import routers
from accounts.views import UserViewSet, user_lsit
from accounts.views import login_api, logout_api, register_api, forgotpassword_api, resetPassword_api, set_googleVerify_api, googleVerify_api, bind_googleVerify_api, unbind_googleVerify_api, change_password_api#, manage_address_api

'''restfulapi router setting'''
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
snippet_list = UserViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

'''simple url settiing'''
urlpatterns = [
    #CRUD
    path('all_list/',alllist),
    path('show/<app>/<table>/',detail),
    path('create/<app>/<table>/',create),
    path('modify/<app>/<table>/<field>/<value>/',modify),
    path('delete/<app>/<table>/<field>/<value>/',terminate),

    re_path(r'^$', index),
    re_path(r'^register/$', register),
    path('<subject>/verify/',email_verify),
    re_path(r'^activate/$', regist_email_verify),
    re_path(r'^login/$', login, name='login'),
    re_path(r'^accounts/login/$', login, name='login'),
    re_path(r'^logout/$', logout),  
    re_path(r'^user_index/$', user_index),
    re_path(r'^google2fa/$', open_2FA),
    re_path(r'^chpwd/$', change_password),
    re_path(r'^forgotpassword/$', forgot_password),




    # route for app Picture
    re_path(r'^pic/', picture),
    # router from snippet_list
    re_path(r'user/', snippet_list, name='snippet_list'),
    re_path(r'^api/', include(router.urls)),
    re_path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^api/user_list', user_lsit),
    # api route
    re_path(r'^login_api', login_api.as_view()),
    re_path(r'^logout_api', logout_api.as_view()),
    re_path(r'^register_api', register_api.as_view()),
    re_path(r'^forgotpassword_api', forgotpassword_api.as_view()),
    re_path(r'^resetPassword_api', resetPassword_api.as_view()),
    re_path(r'^set_googleVerify_api', set_googleVerify_api.as_view()),
    re_path(r'^googleVerify_api', googleVerify_api.as_view()),
    re_path(r'^bind_googleVerify_api', bind_googleVerify_api.as_view()),
    re_path(r'^unbind_googleVerify_api', unbind_googleVerify_api.as_view()),
#    re_path(r'^manage_address_api', manage_address_api.as_view()),
    re_path(r'^change_password_api', change_password_api.as_view()),
]
