from django.db import models


class Coins(models.Model):
    coin = models.CharField(db_column='coin', max_length=5, primary_key=True)
    fullname = models.CharField(db_column='fullname', max_length=20)
    withdrawal_limited = models.FloatField(db_column='withdrawal_limited', max_length=20)
    ex_fee = models.FloatField(db_column='ex_fee', max_length=20)
    img_url = models.CharField(db_column='img_url', max_length=50)
    status = models.CharField(db_column='status', max_length=20, null=True)  # 貨幣狀態、可否提 1都可 2 3 4都不行

    def __str__(self):
        return self.coin


class Exchangeinfos(models.Model):
    symbol = models.CharField(db_column='symbol', max_length=10, primary_key=True)
    status = models.CharField(db_column='status', max_length=20, null=True)
    base_asset = models.CharField(db_column='base_asset', max_length=20, null=True)
    base_asset_precision = models.IntegerField(db_column='base_asset_precision', null=True)
    quote_asset = models.CharField(db_column='quote_asset', max_length=20, blank=True)
    quote_asset_precision = models.IntegerField(db_column='quote_asset_precision', null=True)
    order_type = models.CharField(db_column='order_type', max_length=20, null=True)
    min_price = models.FloatField(db_column='min_price', max_length=20, null=True)
    max_price = models.FloatField(db_column='max_price', max_length=20, null=True)
    min_qty = models.FloatField(db_column='min_qty', max_length=20, null=True)
    max_qty = models.FloatField(db_column='max_qty', max_length=20, null=True)

    def __str__(self):
        return self.symbol


class Fees(models.Model):
    fid = models.BigAutoField(db_column='fid', max_length=100, primary_key=True)
    coin = models.ForeignKey('Coins', on_delete=models.CASCADE)
    name = models.CharField(db_column='name', max_length=20)
    total = models.FloatField(db_column='total', max_length=20, default=0)


class Userlevels(models.Model):
    level = models.IntegerField(db_column='level', primary_key=True)
    discount = models.FloatField(db_column='discount')
    bdb_discount = models.FloatField(db_column='bdb_discount')


class Mails(models.Model):
    subject = models.CharField(db_column='subject', max_length=20, primary_key=True)
    message = models.CharField(db_column='message', max_length=500)
    from_email = models.EmailField(db_column='from_email')
