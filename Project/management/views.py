from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from management import models as management
from accounts import models as accounts
from Picture import models as Picture
from depositandwithdraw import models as depositandwithdraw
from tradeengine import models as tradeengine
from django.apps import apps

from management.forms import get_model_form

def alllist(request):
    html_list=[]
    for a in apps.all_models:
        for b,c in apps.all_models[a].items():
            html_list.append(['%s'%a,'/show/%s/%s'%(a,b),'%s'%b])
    tmp=''
    for a in html_list:
        tmp+='<a href="%s">%s.%s</a><br>'%(a[1],a[0],a[2])
    print(tmp)
    return HttpResponse(tmp)


def detail(request,app,table):
    mod=eval('%s.%s'%(app,table))._meta
    fields=eval('%s.%s'%(app,table))._meta.fields
    data=eval('%s.%s'%(app,table)).objects.values_list()
    vv=data.values(mod.pk.name)
    print(vv)
    '''
    app.model._meta.get_field('xxx')
    has attr below:
    max_length、unique、null、auto_created、is_relation...
    '''
    return render(request, 'detail.html', locals())

def create(request,app,table):
    mod=eval('%s.%s'%(app,table))
    form=get_model_form(mode=mod)
    if request.method == 'POST':
        if form.is_valid():
            user = form.save()
            redirect(detail,app=app,table=table)
    return render(request, "create.html", locals())

def modify(request,app,table,field,value):
    #instance =eval('get_object_or_404(mod, %s=\'%s\')'%(field,value))
    fields=eval('%s.%s'%(app,table))._meta.fields
    pk=eval('%s.%s'%(app,table))._meta.pk.name
    noshowlist=['password','last_login','date_joined']
    try:
        datas=eval('get_object_or_404(%s.%s,%s=\'%s\')'%(app,table,field,value))
    except NameError:
        datas=eval('%s.%s.objects.get(%s=\'%s\')'%(app,table,field,value))
    values={}
    for a in fields:
        isFK=False
        choices={}
        if a.related_model:
            isFK=True
            choices=a.related_model.objects.values_list(a.related_model._meta.pk.name,flat=True)
        dd=eval('datas.%s'%a.name)
        values.update({a.name:{'value':dd,'isFK':isFK,'choices':choices}})
    if request.method == 'POST':
        for a in fields:
            if a.name != pk :
                new_value=request.POST.get(a.name)
                if a.related_model:
                    try:
                        new_value=eval('%s.get(pk=%s)'%(a.related_model.objects,new_value))
                    except NameError:
                        new_value=eval('%s.get(pk=\'%s\')'%(a.related_model.objects,new_value))
                datas.__setattr__(a.name,new_value)
                datas.save()
        return redirect(detail,app=app,table=table)
    return render(request, "modify.html", locals())

def terminate(request,app,table,field,value):
    try:
        datas=eval('%s.%s.objects.get(%s=%s)'%(app,table,field,value))
    except NameError:
        datas=eval('%s.%s.objects.get(%s=\'%s\')'%(app,table,field,value))
    datas.delete()
    return redirect(detail,app=app,table=table)