from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime


class User(AbstractUser):
    phone = models.CharField(db_column='phone', max_length=20, null=True)
    level = models.ForeignKey('management.Userlevels', default=0, on_delete=models.CASCADE) 
    status = models.IntegerField(db_column='status', null=True, default=0)  # 0 1 2 3
    recommendation_code = models.CharField(db_column='recommendation_code', max_length=20, null=True)
    from_recommendation_code = models.CharField(db_column='from_recommendation_code', max_length=20, null=True)
    use_api = models.BooleanField(default=False)
    use_2FA = models.BooleanField(default=False)
    pass_2FA = models.BooleanField(default=False)
    use_SMS = models.BooleanField(default=False)
    pass_SMS = models.BooleanField(default=False)
    use_Whitelist = models.BooleanField(default=False)
    use_bdbfee = models.BooleanField(default=False)
    login_sta = models.BooleanField(default=False)
    mail_sta = models.BooleanField(default=False)
    trade_sta = models.BooleanField(default=False)
    withdraw_sta = models.BooleanField(default=False)
    deposite_sta = models.BooleanField(default=False)

    def time_registered(self):
        dt = self.date_joined.replace(tzinfo=None)
        dt_now = datetime.datetime.now()
        return dt_now-dt

    def __str__(self):
        return self.username


class Balances(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    coin = models.ForeignKey('management.Coins', on_delete=models.CASCADE)
    total = models.FloatField(db_column='total', default=0)
    canuse = models.FloatField(db_column='canuse', default=0)
    freeze = models.FloatField(db_column='freeze', default=0)


class Login_records(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp')
    ip = models.CharField(db_column='ip', max_length=20)
    city = models.CharField(db_column='city', max_length=20)
    status = models.IntegerField(default=0)


class Mail_records(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    subject = models.ForeignKey('management.Mails', on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp')
    ip = models.CharField(db_column='ip', max_length=20)
    url = models.CharField(db_column='url', max_length=50)
    status = models.IntegerField(default=0)  # 0fail 1sending 2sent 3verified 4expired
