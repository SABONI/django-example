from accounts.models import User
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    '''Serializes the Models for restful api framework'''
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'date_joined')
