# Generated by Django 2.1.3 on 2018-11-29 06:39

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('management', '__first__'),
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('phone', models.CharField(db_column='phone', max_length=20, null=True)),
                ('status', models.IntegerField(db_column='status', default=0, null=True)),
                ('recommendation_code', models.CharField(db_column='recommendation_code', max_length=20, null=True)),
                ('from_recommendation_code', models.CharField(db_column='from_recommendation_code', max_length=20, null=True)),
                ('use_api', models.BooleanField(default=False)),
                ('use_2FA', models.BooleanField(default=False)),
                ('pass_2FA', models.BooleanField(default=False)),
                ('use_SMS', models.BooleanField(default=False)),
                ('pass_SMS', models.BooleanField(default=False)),
                ('use_Whitelist', models.BooleanField(default=False)),
                ('use_bdbfee', models.BooleanField(default=False)),
                ('login_sta', models.BooleanField(default=False)),
                ('mail_sta', models.BooleanField(default=False)),
                ('trade_sta', models.BooleanField(default=False)),
                ('withdraw_sta', models.BooleanField(default=False)),
                ('deposite_sta', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('level', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='management.Userlevels')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Balances',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total', models.FloatField(db_column='total', default=0)),
                ('canuse', models.FloatField(db_column='canuse', default=0)),
                ('freeze', models.FloatField(db_column='freeze', default=0)),
                ('coin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='management.Coins')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Login_records',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp')),
                ('ip', models.CharField(db_column='ip', max_length=20)),
                ('city', models.CharField(db_column='city', max_length=20)),
                ('status', models.IntegerField(default=0)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Mail_records',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp')),
                ('ip', models.CharField(db_column='ip', max_length=20)),
                ('url', models.CharField(db_column='url', max_length=50)),
                ('status', models.IntegerField(default=0)),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='management.Mails')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
