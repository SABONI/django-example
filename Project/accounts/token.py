import hashlib
import random
import string



class AccountActivationTokenGenerator():
    
    def random_generator(self, size=8, chars=(string.ascii_uppercase)):
        return ''.join([random.choice(chars) for i in range(size)])
    
    def make_hash_value(self, user, random_str, timestamp):
        m = hashlib.md5()
        data = str(user.id)+random_str+str(timestamp)
        m.update(data.encode("utf-8"))
        h = m.hexdigest()
        return h
    
    def check_token(self, user, random_str, timestamp, token):
        m = hashlib.md5()
        data = str(user.id)+random_str+str(timestamp)
        m.update(data.encode("utf-8"))
        h = m.hexdigest()
        if h==token:
            return True
        return False
account_activation_token = AccountActivationTokenGenerator()
