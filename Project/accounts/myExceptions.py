from django_otp.plugins.otp_totp.models import TOTPDevice

from accounts.models import Login_records, Balances, Mail_records
from management.models import Coins, Mails
from tradeengine.models import Orders
from depositandwithdraw.models import Withdrawals, Deposits


def is_get_object_err(e):
    errs = (Coins.DoesNotExist, Balances.DoesNotExist, Login_records.DoesNotExist, Mail_records.DoesNotExist, Addresses.DoesNotExist, Mails.DoesNotExist,
            Orders.DoesNotExist, Deposits.DoesNotExist, Withdrawals.DoesNotExist, TOTPDevice.DoesNotExist)
    for err in errs:
        if e.__class__ == err:
            msg = e.__str__().split(" ")[0] + ' 物件錯誤，或該筆資料不存在'
            return MyModelObjectDoesNotExistError(msg)
    return False


class MyModelObjectDoesNotExistError(Exception):
    pass


class MyValidationError(Exception):
    pass


class MyMailSendError(Exception):
    pass


class MyConfirmStepError(Exception):
    pass


class MyAggregateError(Exception):
    pass


class MyUserlevelError(Exception):
    pass


class MyCoinMaintenceError(Exception):
    pass
