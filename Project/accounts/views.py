from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib import auth, messages
from django.contrib.auth import get_user_model, update_session_auth_hash, authenticate
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from django.contrib.auth.hashers import check_password
#from django.utils.deprecation import RemovedInDjango21Warning
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from django_otp.plugins.otp_totp.models import TOTPDevice
from django.core.mail import send_mail

from rest_framework import viewsets, parsers
from accounts.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
# from rest_framework.request import Request  # used in annotation

import os
import datetime
import warnings
import secrets
import geoip2.database

from accounts.models import User, Login_records, Balances, Mail_records
from management.models import Coins, Mails
from accounts.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm, SetPasswordForm
from accounts.myExceptions import MyUserlevelError, MyConfirmStepError, MyMailSendError
from accounts.token import account_activation_token


def get_client_ip(request):
    '''gets ip from request meta'''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def register(request):
    if request.method == 'POST':
        try:
            form = UserCreationForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.username=request.POST.get('email')
                user.email = request.POST.get('email')
                user.ip = get_client_ip(request)
                user.is_active=False
                user.save()
                if request.POST.get('swi'):
                    '''pass the emailVerify'''
                    user.is_active=True
                    user.save()
                    return render(request, 'login.html', locals())
                email_send(request, user=user.username, subject='activate')
                return render(request, 'register.html', locals())
            return render(request, 'register.html', locals())
        except Exception as e:  # Exception for exist user or UserCreationForm
            form = UserCreationForm()
            return render(request, 'register.html', locals())
    else:  # Not post
        form = UserCreationForm()
    return render(request, 'register.html', locals())

def email_send(request, user, subject):
    '''sends email ,mail record and configures models data'''
    mail = Mails.objects.get(subject=subject)
    to_user = User.objects.get(username=user)
    t = datetime.datetime.now().timestamp()
    ip = get_client_ip(request)
    url=account_activation_token.random_generator()
    mail_record = Mail_records(user=to_user, subject=mail, timestamp=t, ip=ip,url=url, status=1)
    mail_record.save()
    title = str(mail.subject)+'From '+ip
    code = account_activation_token.make_hash_value(to_user,url,int(t))
    '''sending'''
    msg_plain = render_to_string(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'templates/email.txt'), {"message": mail.message, "rout": subject, "id": str(to_user.pk), "code": code})
    msg_html = render_to_string('email.html', {"message": mail.message, "rout":subject , "id": str(to_user.pk), "code": code})
    try:
        send_mail(title, msg_plain, mail.from_email, [to_user.username], html_message=msg_html,)
        mail_record.status = 2
    except:
        mail_record.status = 0
    mail_record.save()


def email_verify(request,subject):
    '''Verify the regist mail with Get'''
    if request.method == 'GET':
        try:
            user_id = request.GET.get('id')
            emailVerifycode = request.GET.get('emailVerifycode')
            user = User.objects.get(id=int(user_id))
            mail_record = Mail_records.objects.filter(user=user, subject=subject, status=2)
            tmr = mail_record.order_by('-timestamp')[0]
            if account_activation_token.check_token(user,tmr.url,tmr.timestamp,emailVerifycode):
                print('a')
                tmr.status = 3
                tmr.save()
                if subject=='activate':
                    user.is_active=True
                    user.save()
                    for coin in Coins.objects.all():
                        if not Balances.objects.filter(user=user, coin=coin):
                            b = Balances(user=user, coin=coin,)
                            b.save()
                    return redirect(user_index)
                elif subject=='Forgot':
                    print('b')
                    return redirect(resetPassword,user=user)
                print('c')
            else:
                raise Exception
        except Exception as e:
            return HttpResponse('verify failed')

def resetPassword(request,user):
    if request.method == 'POST':
        form = SetPasswordForm(user, request.data)
        if form.is_valid():
            user = form.save()
            return redirect(user_index)
    return render(request,'resetPassword.html',{'form':form})



def regist_email_verify(request,subject):
    '''Verify the regist mail with Get'''
    if request.method == 'GET':
        try:
            user_id = request.GET.get('id')
            emailVerifycode = request.GET.get('emailVerifycode')
            user = User.objects.get(id=int(user_id))
            subject = 'activate'
            mail_record = Mail_records.objects.filter(user=user, subject=subject, status=2)
            tmr = mail_record.order_by('-timestamp')[0]
            if account_activation_token.check_token(user,tmr.url,tmr.timestamp,emailVerifycode):
                tmr.status = 3
                tmr.save()
                user.is_active=True
                user.save()
                for coin in Coins.objects.all():
                    if not Balances.objects.filter(user=user, coin=coin):
                        b = Balances(user=user, coin=coin,)
                        b.save()
            else:
                raise Exception
        except Exception as e:
            return HttpResponse('verify failed')
        return redirect(user_index)


def get_Login_records_data(request):
    data =[]
    for i in Login_records.objects.filter(user=request.user.id).order_by('-timestamp'):
        d = datetime.datetime.fromtimestamp(i.timestamp).strftime('%Y-%m-%d %H:%M:%S')
        data.append({'time':str(d),'IP':i.ip,'city':i.city})
        if len(data)>4:
            break
    return data

def index(request):
    return render(request, 'index.html')


def loginrecord_create(user, ip):
    try:
        gi = geoip2.database.Reader(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/GeoLite2-City.mmdb')
        if ip == "127.0.0.1":
            city = "test"
        else:
            city = gi.city(ip)
        new_loginrecord = Login_records(user=user, timestamp=datetime.datetime.now().timestamp(), city=city, ip=ip)
        new_loginrecord.save()
        return
    except Exception as e:
        raise e

def login(request):
    myview=LoginView.as_view(template_name='login.html', redirect_field_name='next',authentication_form=AuthenticationForm, extra_context=None, redirect_authenticated_user=False)(request)
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        try:
            user = User.objects.get(username=username)
            if check_password(password, user.password):
                loginrecord_create(user, ip=get_client_ip(request))
                if user.login_sta:
                    messages.error(request,'account freezed!')
                    return HttpResponseRedirect('/login/')
                return myview
            messages.error(request,'Incorrect email or incorrect password.')
            return HttpResponseRedirect('/login/')
        except Exception as e:
            messages.error(request,'Error')
            return HttpResponseRedirect('/login/')
    return myview

def googleVerify_required(function):
    '''custom decoration'''
    def googleVerify(request, *args, **kw):
        if request.user.use_2FA:
            uid = request.user.id
            totpd = TOTPDevice.objects.get(user_id=uid)
            if request.method == 'POST':
                #if totpd.verify_token(request.POST.get('facode', False)):
                if 1:
                    return function(request, *args, **kw)
                print('2FA Fail')
                return render(request, 'google2fa.html', locals())
            return render(request, 'google2fa.html', locals())
        else:
            return function(request, *args, **kw)
    return googleVerify

@login_required
@googleVerify_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!update the session.
            messages.success(request, 'Your password was successfully updated!')
            return render(request, 'user_index.html')
        else:
            messages.error(request, 'Please correct the error below.')
            form = PasswordChangeForm(request.user)
            return render(request, 'chpwd.html', {'form': form})
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'chpwd.html', {'form': form})


@login_required
def user_index(request):
    records=get_Login_records_data(request)
    return render(request, 'user_index.html',locals())


def forgot_password(request):
    if request.method == 'POST':
        try:
            username = request.POST.get('mail', None)
            sub = "Forgot"
            user = User.objects.get(username=username)
            '''set the time to restrict
                t = datetime.datetime.now().timestamp()
                if int(t)-int(mr.order_by('-timestamp')[0].timestamp)>60:
                     email_send(request,user_mail=user.username,mail_subject_is=sub,rout="resetPassword")
            '''
            email_send(request, user=user.username, subject=sub)
        except Exception as e:
           raise e
    return render(request, 'forgotpassword.html')


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


'''
def session_test(request):
#    sid = request.COOKIES['sessionid']
    sid='3s3wynnzj3nxe5ln4bcp7bfsx5qoal3h'
#    s_info = 'Session ID:' + sid + '<br>Expire_date:' + str(s.expire_date) + '<br>Data:' + str(s.get_decoded())
    s = Session.objects.all()
    s_info=[]
    for ss in s:
        s_info.append("<li>"+str(ss.get_decoded())+"</li>")
    return HttpResponse(s_info)
'''

@login_required
@googleVerify_required
def open_2FA(request):
    uid = request.user.id
    if  request.user.use_2FA:
        totpd = TOTPDevice.objects.get(user_id=uid)
        qr_url = 'https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl='+totpd.config_url
        note = str(totpd.key)
        if request.method == 'POST':
            if totpd.verify_token(request.POST.get('facode', False)):
                User.objects.filter(pk=uid).update(use_2FA=False)
                totpd.delete()
                msg = "2FA is closed"
                return HttpResponseRedirect('/user_index/')
            print('verify fail')
        return render(request, 'google2fa.html', locals())
    else:
        if TOTPDevice.objects.filter(user_id=uid):
            TOTPDevice.objects.filter(user_id=uid).delete()
        totpd = TOTPDevice.objects.create(user_id=uid)
        qr_url = 'https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl='+totpd.config_url
        note = str(totpd.key)
        if request.method == 'POST':
            if totpd.verify_token(request.POST.get('facode', False)):
                User.objects.filter(pk=uid).update(use_2FA=True)
                msg = "2FA is opened"
                return HttpResponseRedirect('/user_index/')
            print('verify fail')
        return render(request, 'google2fa.html', locals())


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    parser_classes = (parsers.JSONParser,)


@api_view(['GET'])
def user_lsit(request):
    '''use the serializer'''
    if request.method == 'GET':
        snippets = User.objects.all()
        # serializer_context = {
        #     'request': Request(request),
        # }
        # serializer = UserSerializer(snippets, context=serializer_context,many=True)
        serializer = UserSerializer(snippets, context={'request': request}, many=True)
        return JsonResponse(serializer, safe=False)
    if request.method == 'POST':
        snippets = User.objects.all()
        # serializer_context = {
        #     'request': Request(request),
        # }
        # serializer = UserSerializer(snippets, context=serializer_context,many=True)
        serializer = UserSerializer(snippets, context={'request': request}, many=True)
        return JsonResponse(serializer, safe=False)


class CsrfExemptSessionAuthentication(SessionAuthentication):
    '''to clos csrf'''
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class login_api(APIView):
    '''login api with APIView'''
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        try:
            username = request.data['username']
            password = request.data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                user = User.objects.get(username=username)
                if user.is_login:
                    s = Session.objects.all()
                    for ss in s:
                        if ss.get_decoded():
                            ss_expire = ss.expire_date
                            if request.session.session_key == ss.session_key:  # same session
                                if (ss_expire.timestamp()) < (datetime.datetime.now().timestamp()):  # oringal session and is expired
                                    ss.delete()
                                    user.is_login = False
                                    user.pass_2FA = False
                                    user.save()
                                else:  # oringal session and is not expired
                                    user.is_login = False
                                    user.save()
                            elif str(user.id) == str(ss.get_decoded()['member_id']):  # diff session
                                if (ss_expire.timestamp()) < (datetime.datetime.now().timestamp()):  # diff session and is expired
                                    ss.delete()
                                    user.is_login = False
                                    user.pass_2FA = False
                                    user.save()
                                else:  # diff session and is not expired
                                    msg = 'only one session allow'
                                    return HttpResponse(msg)
                    if user.is_login:
                        res = {'status': 'fail'}
                        return JsonResponse(res)
                if user.login_sta:  # check is account freeze or not
                    res = {'status': 'fail'}
                    return JsonResponse(res)
                if check_password(password, user.password):
                    user.is_login = True
                    loginrecord_create(user, ip=get_client_ip(request))
                    user.save()
                    auth.login(request, user)
                    request.session['member_id'] = user.id
                    res = {'status': 'success'}
                    return JsonResponse(res)
                res = {'status': 'fail'}
                return JsonResponse(res)
            else:
                res = {'status': 'fail'}
                return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class logout_api(APIView):
    '''log out current user'''
    def get(self, request):
        try:
            User.objects.filter(username=request.user.username).update(is_login=False)
            User.objects.filter(username=request.user.username).update(pass_2FA=False)
            auth.logout(request)
            res = {'status': 'success'}
            return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class register_api(APIView):
    '''
    the password constrants below:
    1. can't be too similar to your other personal information.
    2. must contain at least 8 characters.
    3. can't be a commonly used password.
    4. can't be entirely numeric.
    '''
    User = get_user_model()
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        try:
            form = UserCreationForm(request.data)
            if form.is_valid():
                user = form.save()
                user.username = request.data['email']
                user.email = request.data['email']
                user.save()
                user.ip = get_client_ip(request)
                '''test without email_send
                subject = 'Verify'
                # email_send(request, user_mail=user.username, mail_subject_is=subject, rout="emailVerify")
                '''
                res = {'status': 'success'}
                return JsonResponse(res)
            res = {'status': 'fail'}
            return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class forgotpassword_api(APIView):
    '''input email and send Forgot email'''
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        try:
            username = request.data['email']
            sub = "Forgot"
            user = User.objects.get(username=username)
            mr = Mail_records.objects.filter(memuuid=user, subject=sub)
            if mr:
                '''set the time to restrict
                t = datetime.datetime.now().timestamp()
                if int(t)-int(mr.order_by('-timestamp')[0].timestamp)>60:
                     email_send(request,user_mail=user.username,mail_subject_is=sub,rout="resetPassword")
                '''
                email_send(request, user_mail=user.username, mail_subject_is=sub, rout="resetPassword")
            else:
                email_send(request, user_mail=user.username, mail_subject_is=sub, rout="resetPassword")
            res = {'status': 'success'}
            return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class resetPassword_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request):
        '''get emailVerifycode update mailrecord status'''
        try:
            user = User.objects.get(pk=request.GET['id'])
            emailVerifycode = request.GET['emailVerifycode']
            subject = "Forgot"
            mr = Mail_records.objects.filter(memuuid=user, subject=subject, status=2)
            tmr = mr.order_by('-timestamp')[0]
            code = Myencode(user.username, tmr.timestamp, tmr.url)
            if not Myencode_validate(emailVerifycode, user.username, tmr.timestamp, tmr.url):
                raise MyUserlevelError("email verify failed" + str(user.username) + str(tmr.timestamp) + str(tmr.url) + code)
            tmr.status = 3
            tmr.save()
            res = {'status': 'success'}
            return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)

    @csrf_exempt
    def post(self, request):
        '''
        post SetPasswordForm(new_password1,new_password2) update mailrecord status.
        The new password can't be too similar to your other personal information or old password.
        '''
        try:
            user = User.objects.get(pk=request.GET['id'])
            subject = "Forgot"
            mr = Mail_records.objects.filter(memuuid=user, subject=subject, status=3)
            tmr = mr.order_by('-timestamp')[0]
            if tmr:
                form = SetPasswordForm(user, request.data)
                if form.is_valid():
                    user = form.save()
                    tmr.status = 4
                    tmr.save()
                    update_session_auth_hash(request, user)  # Important!
                    res = {'status': 'success'}
                    return JsonResponse(res)
                else:
                    res = {'status': 'fail'}
                    return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class googleVerify_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        '''post facode ->2FA. pass_2FA will be False before check'''
        try:
            if request.user.id:
                if request.user.use_2FA:
                    if not request.user.pass_2FA:
                        uid = request.user.id
                        totpd = TOTPDevice.objects.get(user_id=uid)
                        User.objects.filter(pk=uid).update(pass_2FA=False)
                        if totpd.verify_token(request.data['facode']):
                            User.objects.filter(pk=uid).update(pass_2FA=True)
                            res = {'status': 'success'}
                            return JsonResponse(res)
                        res = {'status': 'fail'}
                        return JsonResponse(res)
                res = {'status': 'doesn\'t use 2FA !'}
                return JsonResponse(res)
            res = {'status': 'login first !'}
            return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)


class set_googleVerify_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request):
        '''generate totpd'''
        if request.user.id:
            uid = request.user.id
            try:
                totpd = TOTPDevice.objects.get(user_id=uid)
                totpd.delete()
                totpd_new = TOTPDevice.objects.create(user_id=uid)
                qr_url = 'https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=' + totpd_new.config_url
                note = str(totpd_new.key)
                res = {'status': 'account has already got googleVerify!!', 'qr_url': qr_url, 'note': note}
                return JsonResponse(res)
            except Exception as e:
                totpd = TOTPDevice.objects.create(user_id=uid)
                qr_url = 'https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=' + totpd.config_url
                note = str(totpd.key)
                res = {'status': 'new googleVerify!!', 'qr_url': qr_url, 'note': note}
                return JsonResponse(res)
        res = {'status': 'login first !'}
        return JsonResponse(res)


class bind_googleVerify_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        '''post password ,2fa-code to bind the TOTPD'''
        print(request.user)
        if request.user.id:
            try:
                uid = request.user.id
                totpd = TOTPDevice.objects.get(user_id=uid)
                password = request.data['password']
                user = User.objects.get(pk=uid)
                if totpd.verify_token(request.data['facode']):
                    if check_password(password, request.user.password):
                        user.use_2FA = True
                        user.save()
                        res = {'status': 'success !'}
                        return JsonResponse(res)
                    else:
                        res = {'status': 'post wrong data !'}
                        return JsonResponse(res)
                else:
                    res = {'status': 'post wrong data !'}
                    return JsonResponse(res)
            except Exception as e:
                res = {'status': 'exception', 'msg': e.__class__.__name__}
                return JsonResponse(res)
        res = {'status': 'login first !'}
        return JsonResponse(res)


class unbind_googleVerify_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        '''post password ,2fa-code to unbind the TOTPD'''
        print(request.user)
        if request.user.id:
            try:
                uid = request.user.id
                totpd = TOTPDevice.objects.get(user_id=uid)
                password = request.data['password']
                user = User.objects.get(pk=uid)
                if totpd.verify_token(request.data['facode']):
                    if check_password(password, request.user.password):
                        user.use_2FA = False
                        user.save()
                        res = {'status': 'success !'}
                        return JsonResponse(res)
                    else:
                        res = {'status': 'post wrong data !'}
                        return JsonResponse(res)
                else:
                    res = {'status': 'post wrong data !'}
                    return JsonResponse(res)
            except Exception as e:
                res = {'status': 'exception', 'msg': e.__class__.__name__}
                return JsonResponse(res)
        res = {'status': 'login first !'}
        return JsonResponse(res)


class change_password_api(APIView):
    parser_classes = [parsers.JSONParser, ]
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @csrf_exempt
    def post(self, request):
        '''post old_password new_password1 new_password2 to change password '''
        try:
            if request.user.id:
                user = User.objects.get(pk=request.user.id)
                form = PasswordChangeForm(user, request.data)
                if form.is_valid():
                    user = form.save()
                    update_session_auth_hash(request, user)  # Important!
                    res = {'status': 'success !'}
                    return JsonResponse(res)
                else:
                    res = {'status': 'fail !'}
                    return JsonResponse(res)
            else:
                res = {'status': 'login first !'}
                return JsonResponse(res)
        except Exception as e:
            res = {'status': 'exception', 'msg': e.__class__.__name__}
            return JsonResponse(res)
